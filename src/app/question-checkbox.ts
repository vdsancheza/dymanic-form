/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T19:59:24-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question-checkbox.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-25T11:54:00-05:00
 */
 import { QuestionBase } from './question-base';

 export class CheckboxControl extends QuestionBase<string>{
   SimpleDataType = 'Boolean';
   type:string;

   constructor(options:{} = {}){
     super(options);
     this.type = options['type'] || '';
   }
 }
