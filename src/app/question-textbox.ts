/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question-textbox.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-25T11:52:55-05:00
 */



import { QuestionBase } from './question-base';

export class TextboxQuestion extends QuestionBase<string> {
  SimpleDataType = 'String' || 'Caracter';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
