/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question-base.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-25T11:52:06-05:00
 */



export class QuestionBase<T>{

  ACL: number; //
  Description: string; //
  Field: string; //  key
  Groups: string; //
  Hidden: boolean; //
  Label: string;  //
  Required: boolean;  //
  SimpleDataType: string; // controlType
  SugMaxLength: number; //

  value: T;
  order: number;  // Prioridad de Impresion.

  constructor(options: {
      ACL?: number,
      Description?: string,
      Field?: string,
      Groups?: string,
      Hidden?: boolean,
      Label?: string,
      Required?: boolean,
      SimpleDataType?: string,
      SugMaxLength?: number,

      value?: T,
      order?: number,
    } = {}) {
    this.ACL = options.ACL || 0;
    this.Description = options.Description || '';
    this.Field = options.Field || '';
    this.Groups = options.Groups || '';
    this.Hidden = !!options.Hidden;
    this.Label = options.Label || '';
    this.Required = !!options.Required;
    this.SimpleDataType = options.SimpleDataType || '';
    this.SugMaxLength = options.SugMaxLength || 100;

    this.value = options.value;
    this.order = options.order === undefined ? 1 : options.order;    
  }
}
