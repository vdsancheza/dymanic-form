/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: app.component.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-26T10:15:39-05:00
 */



import { Component, OnInit }       from '@angular/core';

import { QuestionService } from './question.service';
import { HttpService } from "./http.service";

@Component({
  selector: 'app-root',
  template: `
    <div>
      <h2>Test Form Dynamically</h2>
      <app-dynamic-form [questions]="questions"></app-dynamic-form>
    </div>
  `,
  providers:  [QuestionService, HttpService]
})
export class AppComponent implements OnInit{
  questions: any[];
  template : any[];
  constructor(private service: QuestionService) {
    this.test();
  }

ngOnInit() {
  /*this.service.getTemplate().subscribe(success => {
    this.template = JSON.parse(success['TemplateConfig']).Fields;
    this.service.getFieldToPrint(this.template);
    console.log('templateOnInit', this.template);
  });*/

}

test(){

  //console.log('Template AppCOmponent', this.template);
  this.questions = this.service.getQuestions();
  //console.log('Questions:',this.questions);
}




}
