/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question.service.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-26T10:15:12-05:00
 */



import { Injectable, OnInit } from '@angular/core';



import { DropdownQuestion } from './question-dropdown';
import { QuestionBase } from './question-base';
import { TextboxQuestion } from './question-textbox';
import { CheckboxControl } from "./question-checkbox";

import { HttpService } from './http.service';

@Injectable()
export class QuestionService {

  public template: any;
  public url = 'https://qor.mlsvow.com/Template/' + 28;
  public fieldsToRender = [];

  constructor(private _http: HttpService) {

  }
  // Todo: get from a remote source of question metadata
  // Todo: make asynchronous
  //
  //
  //
  /*getTemplate() {
    return this._http.get(this.url);
  }

  getFieldToPrint(template: any) {

    console.log('Entro a getFieldToPrint',template);
    for(let i = 0 ; i < template.length ; ++i){
      switch(template[i].SimpleDataType){

        case 'Number':
              this.fieldsToRender.push(
                new TextboxQuestion({
                  Field: template[i].Field,
                  Label: template[i].Label,
                  Required: template[i].Required,
                  type: template[i].SimpleDataType,
                })
              );

        break;

        case ('String' || 'Caracter'):
              this.fieldsToRender.push(
                new TextboxQuestion({
                  Field: template[i].Field,
                  Label: template[i].Label,
                  Required: template[i].Required,
                  Hidden : template[i].Hidden
                })
              );

        break;

        case 'String List, Single':
              this.fieldsToRender.push(
                new DropdownQuestion({
                  Field: template[i].Field,
                  Label: template[i].Label,
                  Required: template[i].Required,
                  Hidden : template[i].Hidden,
                  options: [ //Aqui deberia llamarse a los lookups
                    { Field: 'solid', value: 'Solid' },
                    { Field: 'great', value: 'Great' },
                    { Field: 'good', value: 'Good' },
                    { Field: 'unproven', value: 'Unproven' }
                  ],
              }));

        break;

        case 'Boolean':
              this.fieldsToRender.push(
                new CheckboxControl({
                  Field: template[i].Field,
                  Label: template[i].Label,
                  type: 'Boolean',
                  Required: template[i].Required,
                  Hidden : template[i].Hidden
                })
              );

        break;

        case 'Date':

        break;
      }
    }
    console.log('template',template,'fieldsToRender',this.fieldsToRender);
  }*/

  getQuestions( ) {

    //this.getFieldToPrint();
    let questions: QuestionBase<any>[] =
      [
        new DropdownQuestion({
          Field: 'brave',
          Label: 'Bravery Rating',
          options: [
            { Field: 'solid', value: 'Solid' },
            { Field: 'great', value: 'Great' },
            { Field: 'good', value: 'Good' },
            { Field: 'unproven', value: 'Unproven' }
          ],

        }),

        new TextboxQuestion({
          Field: 'firstName',
          Label: 'First name',
          value: 'Bombasto',
          Required: true,
          SugMaxLength: 25
        }),
        new TextboxQuestion({
          Field: 'emailAddress',
          Label: 'Email',
          type: 'email',
          Required: true
        }),
        new TextboxQuestion({
          Field: 'LastName',
          Label: 'Last Name',
          Required: true,
          SugMaxLength: 3
        }),
        new TextboxQuestion({
          Field: 'age',
          Label: 'age',
          Required: true,
          type: 'Number',
        }),

        new CheckboxControl({
          Field: 'agree',
          Label: 'I Agree',
          type: 'checkbox',
          Required: false
        })
      ];
    return questions.sort((a, b) => a.order - b.order);
  }
}
