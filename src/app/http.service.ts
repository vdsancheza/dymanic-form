/**
 * @Author: lacm
 * @Date:   2018-03-05T15:13:12-05:00
 * @Email:  lacmdeveloper@gmail.com
 * @Filename: http.service.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-24T20:14:46-05:00
 */

import { Injectable } from '@angular/core';

// Angular http libraries
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class HttpService {

  /**
   * @param  {HttpClient} _http [ local variable to use the HttpClient functions ]
   */
  constructor(
    public _http: HttpClient
  ) {

  }

  private cleanHeaders = () => ({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/x-www-form-urlencoded',
    'OSN': 'demo-app'
  });

  private createHeaders = (tokens: any = null) => {
    const custom_headers = this.cleanHeaders();

    if (tokens !== null) {
      if (
        tokens.hasOwnProperty('refresh') &&
        tokens.refresh != null &&
        tokens.refresh != 'undefined' &&
        tokens.refresh != '' &&
        tokens.hasOwnProperty('authorization') &&
        tokens.authorization != null &&
        tokens.authorization != 'undefined' &&
        tokens.authorization != ''
      ) {
        custom_headers['refresh_token'] = tokens.refresh;
        custom_headers['authorization'] = tokens.authorization;
      } else if (
        tokens.hasOwnProperty('authorization') &&
        tokens.authorization != null &&
        tokens.authorization != 'undefined' &&
        tokens.authorization != '' &&
        tokens.hasOwnProperty('openid') &&
        tokens.openid != null &&
        tokens.openid != 'undefined' &&
        tokens.openid != ''
      ) {
        custom_headers['authorization'] = tokens.authorization;
        custom_headers['openid'] = tokens.openid;
      } else if (
        tokens.hasOwnProperty('authorization') &&
        tokens.authorization != null &&
        tokens.authorization != 'undefined' &&
        tokens.authorization != ''
      ) {
        // TEMPORARY CONDITION
        // WHEN WE ELIMINATE COMPLETELY THE NEED OF USING OPENID
        custom_headers['authorization'] = tokens.authorization;
      }
    }

    return new HttpHeaders(custom_headers);
  }

  /**
   * Httpclient get method
   * @param  {string} 			url    		[ Accepts a string as url]
   * @param  {any} 					params 		[ OPTIONAL if the param value is present can be any. To be send to the API ]
   * @param  {any} 					tokens		[ Special parameter to handle api tokens ]
   * @return {Obervable}        			[ Returns the http get call as an observable ]
   */
  public get = (url: string, params: any = null, tokens: any = null) => {
    return this._http.get(url, {
      headers: this.createHeaders(tokens),
      params: params
    });
  }

  /**
   * Httpclient post method
   * @param  {string}       url    		[ Accepts a string as url ]
   * @param  {any}          params 		[ The params to be send to the API ]
   * @param  {any} 					tokens		[ Special parameter to handle api tokens ]
   * @return {Observable}             [ Returns the http post call as an observable ]
   */
  public post = (url: string, params: any = null, tokens: any = null) => {
    return this._http.post(url, params, {
      headers: this.createHeaders(tokens)
    });
  }

  /**
   * Http put method
   * @param  {string}          url    	[ Accepts a string as url ]
   * @param  {any}          	params 		[ OPTIONAL if the param value is present can be any. To be send to the API ]
   * @param  {any} 						tokens		[ Special parameter to handle api tokens ]
   * @return {Observable}               [ Returns the http put call as an observable ]
   */
  public put = (url: string, params: any = null, tokens: any = null) => {
    return this._http.put(url, params, {
      headers: this.createHeaders(tokens)
    });
  }

  /**
   * Http patch request
   * @param  url      [description]
   * @param  params   [description]
   * @return          [description]
   */
  public patch = (url: string, params: any) => {
    /*
     * TO BE IMPLEMENTED
     */
  }

	/**
	 * Http delete request
	 * @param  {string} 	url      		[ URl needed for the http call ]
	 * @param  {any}			params   		[ Optional parameter for the request ]
	 * @param  {any} 			tokens			[ Special parameter to handle api tokens ]
	 * @return          							[ Http delete observable ]
	 */
  public delete = (url: string, params: any = null, tokens: any = null) => {
    return this._http.delete(url, {
      headers: this.createHeaders(tokens),
      params: params
    });
  }

  /**
   * This function convert a typescript objecto into x-www-form-urlencoded
   * @param  {Object} 	obj 	[ The object to be encoded]
   * @return {string}         [Return the encoded object as a string ]
   */
  public convertObject = (obj) => {
    let str = '';
    for (const key in obj) {
      if (str !== '') {
        str += '&';
      }
      str += key + '=' + encodeURIComponent(obj[key]);
    }

    return str;
  }

}
