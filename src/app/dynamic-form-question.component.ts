/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-25T00:10:20-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: dynamic-form-question.component.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-26T08:57:46-05:00
 */



import { Component, Input } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { QuestionBase }     from './question-base';
import { HttpService } from "./http.service";

@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html',
  providers: [ HttpService ]
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() {
    return this.form.controls[this.question.Field].valid;
  }
}
