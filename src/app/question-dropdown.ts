/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question-dropdown.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-25T11:53:17-05:00
 */



import { QuestionBase } from './question-base';

export class DropdownQuestion extends QuestionBase<string> {
  SimpleDataType = 'String List, Single';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}
