/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: question-control.service.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-26T08:55:03-05:00
 */



import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { QuestionBase } from './question-base';

@Injectable()
export class QuestionControlService {
  constructor() { }

  toFormGroup(questions: QuestionBase<any>[] ) {
    let group: any = {};

    questions.forEach(question => {
      group[question.Field] = question.Required ? new FormControl(question.value || '', [Validators.required, Validators.maxLength(question.SugMaxLength)])
                                              : new FormControl(question.value || '', Validators.maxLength(question.SugMaxLength));
    });
    return new FormGroup(group);
  }
}
