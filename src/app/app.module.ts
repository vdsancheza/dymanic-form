/**
 * @Author: Victor D. Sanchez A. <victors>
 * @Date:   2018-03-24T15:03:38-05:00
 * @Email:  victorsanchez1993@gmail.com
 * @Filename: app.module.ts
 * @Last modified by:   victors
 * @Last modified time: 2018-03-25T00:22:30-05:00
 */



import { BrowserModule }                from '@angular/platform-browser';
import { ReactiveFormsModule }          from '@angular/forms';
import { NgModule }                     from '@angular/core';

import { AppComponent }                 from './app.component';
import { DynamicFormComponent }         from './dynamic-form.component';
import { DynamicFormQuestionComponent } from './dynamic-form-question.component';
import { HttpService } from "./http.service";
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [ BrowserModule, ReactiveFormsModule, HttpModule, HttpClientModule ],
  declarations: [ AppComponent, DynamicFormComponent, DynamicFormQuestionComponent ],
  bootstrap: [ AppComponent ],
  providers: [ HttpService, HttpClientModule ]
})
export class AppModule {
  constructor() {
  }
}
